# Tag structure builder and parser

Author: [Peter Peto](http://petopeter.hu)


PHP based HTML and XML tag builder and parser

## Tech

PHP based
Tag structure uses a projects to work properly:
* [Tag builder and parser](https://bitbucket.org/petop/tag-builder-and-parser) - PHP based HTML and XML tag builder and parser

## Installation

Tag builder and parser requires [PHP](https://www.php.net/) v5.3.0+ to run.

You need to add this lines into your composer.json:
```sh
"repositories": [
    {
        "type": "vcs",
        "url":  "https://bitbucket.org/petop/tag-structure-builder-and-parser.git"
    }
]
"require": {
    "petop/tag-structure-builder-and-parser": "dev-master"
},
```

To install the defined dependencies for your project, run the `install` command.

```sh
$ php composer.phar update
$ php composer.phar install
```

## Usage

### Parse tag string
```php
$structure = new TagStructure();
$structure->parseTagString('<document><user>John <p></p>Doe</user><email>john@example.com</email></document>');
```

### Parse file
```php
$path = './example.xml';
$structure = new TagStructure();
$structure->parseTagFile($path);
```

### Add header (/prefix, for the xml of example)
```php
$xmlsString = "<document><user>John <p></p>Doe</user><email>john@example.com</email></document>";
$structure = new TagStructure();
$structure->parseTagString($xmlsString);
$structure->addHeader('<?xml version="1.0" encoding="UTF-8"?>');
```

### Get header
```php
$structure->getHeader();
```
`result: <?xml version="1.0" encoding="UTF-8"?>`

### Get Structure
```php
$structure->getStructure();
```
`instead of result: PeterPeto\Tag`

### Get structure like string
```php
$structure->__toString();
```
### Echo structure like string
```php
echo $structure;
```

## Licence Information
CC-BY-NC-SA
- ## You are free to:
  - **Share** — copy and redistribute the material in any medium or format
  - **Adapt** — remix, transform, and build upon the material
  > The licensor cannot revoke these freedoms as long as you follow the license terms.

- ## Under the following terms:
  - **Attribution** — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use. 
  - **NonCommercial** — You may not use the material for commercial purposes.
  - **ShareAlike** — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
  - **No additional restrictions** — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
  > Notices:
You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.
No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.
