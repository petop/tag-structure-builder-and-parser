<?php
declare(strict_types = 1);

use \PHPUnit\Framework\TestCase;
use PeterPeto\TagStructure;

class TagStructureTest extends TestCase
{
    private $testXmlFilesDirPath = __DIR__ . DIRECTORY_SEPARATOR . 'test_xml_files' . DIRECTORY_SEPARATOR;

    /**
     * @expectedException PHPUnit\Framework\Error\Error
     */
    public function testParseTagStringArgumentCountError()
    {
        $this->expectException(ArgumentCountError::class);
        //$this->expectErrorMessage('Can not connect to database');
        $tagStructure = new TagStructure();
        $tagStructure->parseTagString();
    }

    public function testparseTagFileArgumentCountError()
    {
        $this->expectException(ArgumentCountError::class);
        $tagStructure = new TagStructure();
        $tagStructure->parseTagFile();
    }

    public function testparseTagFileFileNotFoundError()
    {
        $path = '0123456789\not_exists_file_path.xml';
        $this->expectError();
        $this->expectErrorMessage('File not found: ' . (string)$path);
        $tagStructure = new TagStructure();
        $tagStructure->parseTagFile($path);
    }

    public function testTagStringClenanerEmptyString()
    {
        $tagStructure = new TagStructure();
        $this->assertEquals($tagStructure, "");
    }

    public function testParseTagStringWithOnlyHeader()
    {
        $header = "<?xml version='1.0' encoding='UTF-8'?>";
        $tagStructure = new TagStructure();
        $tagStructure->parseTagString($header);

        $this->assertEquals($header, $tagStructure);
    }

    public function testParseTagStringWithOnlyTwoHeader()
    {
        $header = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><?mso-application progid="Word.Document"?>';
        $tagStructure = new TagStructure();
        $tagStructure->parseTagString($header);

        $this->assertEquals($header, $tagStructure);
    }

    public function testParseTagStringWithOnlyTwoHeaderAndWhiteSpaces()
    {
        $header = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' . "\n\r\t"  . '<?mso-application progid="Word.Document"?>';
        $tagStructure = new TagStructure();
        $tagStructure->parseTagString($header);

        $assert = str_replace(["\t","\n","\r"], '', $header);
        $this->assertEquals($assert, $tagStructure);
    }

    public function testParseTagStringWithMainTag()
    {
        $exampleXml = "<?xml version='1.0' encoding='UTF-8'?><document></document>";
        $tagStructure = new TagStructure();
        $tagStructure->parseTagString($exampleXml);

        $assert = $exampleXml;
        $this->assertEquals($assert, $tagStructure);
    }

    public function testParseTagStringWithOneEmptySubTag()
    {
        $exampleXml = "<?xml version='1.0' encoding='UTF-8'?><document><user/></document>";
        $tagStructure = new TagStructure();
        $tagStructure->parseTagString($exampleXml);

        $assert = $exampleXml;
        $this->assertEquals($assert, $tagStructure);
    }

    public function testParseTagStringWithOneNoneEmptySubTag()
    {
        $exampleXml = "<?xml version='1.0' encoding='UTF-8'?><document><user>John Doe</user></document>";
        $tagStructure = new TagStructure();
        $tagStructure->parseTagString($exampleXml);

        $assert = $exampleXml;
        $this->assertEquals($assert, $tagStructure);
    }

    public function testParseTagStringWithOneNoneEmptySubTags()
    {
        $exampleXml = "<?xml version='1.0' encoding='UTF-8'?><document><user>John <p></p>Doe</user><email>john@example.com</email></document>";
        $tagStructure = new TagStructure();
        $tagStructure->parseTagString($exampleXml);

        $assert = $exampleXml;
        $this->assertEquals($assert, $tagStructure);
    }

    public function testEmptyOOXMLParse()
    {
        $header = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><?mso-application progid="Word.Document"?><pkg:package xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage"></pkg:package>';
        $tagStructure = new TagStructure();
        $tagStructure->parseTagString($header);

        $assert = str_replace(["\t","\n","\r"], '', $header);
        $this->assertEquals($assert, $tagStructure);
    }

    public function testOOXMLSmallPieceParse()
    {
        $header = file_get_contents($this->testXmlFilesDirPath . 'small_test_ooxml.xml');
        $tagStructure = new TagStructure();
        $tagStructure->parseTagString($header);

        $assert = str_replace(["\t","\n","\r"], '', $header);
        $this->assertEquals($assert, $tagStructure);
    }

    public function testOOXMLSmallFileParse()
    {
        $path = $this->testXmlFilesDirPath . 'small_test_ooxml.xml';

        $tagStructure = new TagStructure();
        $tagStructure->parseTagFile($path);

        $assert = str_replace(["\t","\n","\r"], '', file_get_contents($path));
        $this->assertEquals($assert, $tagStructure);   
    }

    public function testOOXMLMediumPieceParse()
    {
        $header = file_get_contents($this->testXmlFilesDirPath . 'medium_test_ooxml.xml');
        $tagStructure = new TagStructure();
        $tagStructure->parseTagString($header);

        $assert = str_replace(["\t","\n","\r"], '', $header);
        $this->assertEquals($assert, $tagStructure);
    }

    public function testOOXMLMediumFileParse()
    {
        $path = $this->testXmlFilesDirPath . 'medium_test_ooxml.xml';

        $tagStructure = new TagStructure();
        $tagStructure->parseTagFile($path);

        $assert = str_replace(["\t","\n","\r"], '', file_get_contents($path));
        $this->assertEquals($assert, $tagStructure);   
    }

    public function testOOXMLParse()
    {
        $header = file_get_contents($this->testXmlFilesDirPath . 'test_ooxml.xml');
        $tagStructure = new TagStructure();
        $tagStructure->parseTagString($header);

        $assert = str_replace(["\t","\n","\r"], '', $header);
        $this->assertEquals($assert, $tagStructure);
    }

    public function testOOXMLFileParse()
    {
        $path = $this->testXmlFilesDirPath . 'test_ooxml.xml';

        $tagStructure = new TagStructure();
        $tagStructure->parseTagFile($path);

        $assert = str_replace(["\t","\n","\r"], '', file_get_contents($path));
        $this->assertEquals($assert, $tagStructure);   
    }

    public function testXMLFileParse()
    {
        $path = $this->testXmlFilesDirPath . 'test_xml.xml';

        $tagStructure = new TagStructure();
        $tagStructure->parseTagFile($path);

        $assert = str_replace(["\t","\n","\r"], '', file_get_contents($path));
        $this->assertEquals($assert, $tagStructure);   
    }

    public function testLoremIpsumXMLFileParse()
    {
        $path = $this->testXmlFilesDirPath . 'lorem-ipsum.xml';

        $tagStructure = new TagStructure();
        $tagStructure->parseTagFile($path);

        $assert = str_replace(["\t","\n","\r"], '', file_get_contents($path));
        $this->assertEquals($assert, $tagStructure);   
    }

    public function testEdgarAllanPoeXMLFileParse()
    {
        $path = $this->testXmlFilesDirPath . 'edgar-allan-poe-valogatott-muvei.xml';

        $tagStructure = new TagStructure();
        $tagStructure->parseTagFile($path);

        $assert = str_replace(["\t","\n","\r"], '', file_get_contents($path));
        $this->assertEquals($assert, $tagStructure);   
    }
}
