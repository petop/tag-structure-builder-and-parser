<?php
namespace PeterPeto;

use PeterPeto\Tag;

class TagStructure {
    private $xmlHeader = "";
    private  $xmlString = "";
    private $tagStructure = null;

    public function __toString()
    {
        if(empty($this->tagStructure))
        {
            return $this->xmlHeader;
        }

        return $this->xmlHeader . $this->tagStructure->__toString();
    }

    public function parseTagString($xmlString)
    {
        $this->xmlHeader = "";
        $this->tagStructure = null;

        $this->xmlString = str_replace(["\t","\n","\r"], '', $xmlString);
        $this->cutHeader();
        $this->tagStructure = $this->getXmlStucture();
    }

    public function parseTagFile($path, $newPath = null)
    {
        $xmlString = $this->readXmlFile($path);
        if($xmlString == false) {
            return $this->defultErrorCode;
        }
        
        $this->parseTagString($xmlString);
        
        if(empty($newPath)) {
            return $this;
        }

        return $this->writeXmlFile($this->__toString(), $newPath) == true ?: $this->defaultErrorCode;
    }

    public function addHeader(string $header)
    {
        $this->xmlHeader .= $header;
    }

    public function getHeader()
    {
        return $this->xmlHeader;
    }
    

    public function getStructure()
    {
        return $this->tagStructure;
    }

    private function readXmlFile($path)
    {
        if(!file_exists($path)) {
            \trigger_error("File not found: " . (string)$path);
            return false;
        }

        $content = file_get_contents($path);
        
        return $content;
    }

    private function writeXmlFile($xmlString, $path)
    {
        if(file_exists($path) && !is_dir($path) && !unlink($path))
        {
            \trigger_error('Access denied: unable delete file: ' . (string)$path);
            return false;
        }

        return file_put_contents($path, $xmlString) !== false;
    }

    private function cutHeader()
    {
        $xmlString = &$this->xmlString;
        while(substr($xmlString, 0, 2) == '<?')
        {
            $index = strpos($xmlString, '>') + 1;
            $this->xmlHeader .= substr($xmlString, 0, $index);
            $xmlString = substr($xmlString, $index);
        }
    }

    private function getXmlStucture() : ?Tag
    {
        $xmlString = &$this->xmlString;
        $tagStructure = null;

        $isOdd = FALSE;
        while(strlen($xmlString) > 0 && substr($xmlString, 0, 1) == '<' && substr($xmlString, 0, 2) !== '</' && $isOdd == FALSE)
        {
            $endCharIndex = strpos($xmlString, '>');
            $tagString = substr($xmlString, 0, $endCharIndex + 1);
            $tagStructure = new Tag($tagString);
            $xmlString = substr($xmlString, strlen($tagString));

            if(($content = $this->getContent()) !== FALSE)
            {
                $tagStructure->addContent($content);
            }

            while(strlen($xmlString) > 0 && $tagStructure->isOdd() == FALSE && ($tag = $this->getXmlStucture()) !== NULL)
            {
                $tagStructure->addTag($tag);
            }

            if(($content = $this->getContent()) !== FALSE)
            {
                $tagStructure->addContent($content);
            }

            $isOdd = $tagStructure->isOdd();
        }

        if(is_null($tagStructure) == FALSE && $tagStructure->isOdd() == FALSE && substr($xmlString, 0, 2) === '</')
        {
            $endCharIndex = strpos($xmlString, '>');
            $tagString = substr($xmlString, 0, $endCharIndex + 1);
            $xmlString = substr($xmlString, strlen($tagString));
        }

        return $tagStructure;
    }

    private function getContent()
    {
        $xmlString = &$this->xmlString;

        if(strlen($xmlString) == 0 || in_array(substr($xmlString, 0, 1), ['<', '>'])) {
            return FALSE;
        }

        $index = strpos($xmlString, '<');
        $content = $index ? substr($xmlString, 0, $index) : $xmlString;
        
        $xmlString = substr($xmlString, strlen($content));

        return $content;
    }
}
